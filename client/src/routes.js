import React from "react";
import {Switch, Route, Redirect} from 'react-router-dom';
import {LinksPage} from "./pages/LinksPage";
import {CreatePage} from "./pages/CreatePage";
import {DeatilPage} from "./pages/DetailPage";
import {AuthPage} from "./pages/AuthPage";

export const useRoutes = (isAuthenticated) => {
	if (isAuthenticated) {
		return (
		<Switch>
			<Route path="/links" exact>
				<LinksPage/>
			</Route>
			<Route path="/create" exact>
				<CreatePage/>
			</Route>
			<Route path="/detail/:id">
				<DeatilPage />
			</Route>
			<Redirect to="/create"/>
		</Switch>
		)
	}

	return <Switch>
		<Route path="/login">
			<AuthPage/>
		</Route>
		<Route path="/register">
			<AuthPage/>
		</Route>
		<Redirect to="/login" />
	</Switch>
}