import React, {useContext, useEffect, useState} from "react";
import {useHttp} from "../hooks/http.hooks";
import {useMessage} from "../hooks/message.hooks";
import {AuthContext} from "../context/AuthContext";

export const AuthPage = (props) => {
	const auth = useContext(AuthContext)
	const message = useMessage();
	const [form, setForm] = useState({email:null,password: null})
	const {loading, error, clearError, request} = useHttp();
	const changeHandler = event => setForm({...form, [event.target.name]: event.target.value });

	useEffect(()=> {
		window.M.updateTextFields();
	}, [])

	useEffect(()=> {
		message(error)
		clearError()
	}, [error, clearError, message])

	const registerHandler = async (e) => {
		e.preventDefault()
		try {
			const data = await request('/api/auth/register', 'POST', {...form})
			message(data.message)
			console.log('Data', data)
		}catch (e) {}
	}
	const loginHandler = async (e) => {
		e.preventDefault()
		try {
			const data = await request('/api/auth/login', 'POST', {...form})
			auth.login(data.token, data.userId)
			console.log('Data', data)
		}catch (e) {}
	}

	return <div className="row">
		<div className="col s6 offset-s3">
			<h1>Сократи ссылку</h1>
			<div className="card blue darken-1">
				<div className="card-content white-text">
					<span className="card-title"> {loading} Авиторизация</span>
					<div>
						<div className="input-field">
							<input type="text"
										 placeholder="Введите email"
										 id="email"
										 name="email"
										 value={form.email}
										 onChange={changeHandler}
										 className="yellow-input"
							/>
							<label htmlFor="email">Email</label>
						</div>
						<div className="input-field">
							<input type="text"
										 placeholder="Введите пароль"
										 id="password"
										 name="password"
										 value={form.password}
										 onChange={changeHandler}
										 className="yellow-input"
							/>
							<label htmlFor="password">Password</label>
						</div>
					</div>
				</div>
				<div className="card-action">
					<button className="btn yellow darken-4"
									style={{marginRight: 10}}
									onClick={loginHandler}
									disabled={loading}>Войти</button>
					<button className="btn grey lighten-1 black-text"
									onClick={registerHandler}
									disabled={loading}>Регистрация</button>
				</div>
			</div>
		</div>
	</div>
};