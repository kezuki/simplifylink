const express = require('express');
const config = require('config');
const path = require('path');
const mongoose = require('mongoose');

const app = express();
const PORT = config.get('PORT') || '5007';

app.use(express.json({extended: true}))
app.use('/api/auth', require('./routes/auth.routes'));
app.use('/api/link', require('./routes/link.routes'));
app.use('/t', require('./routes/redirect.routes'));
if (process.env.NODE_ENV === 'production') {

	app.use('/', express.static(path.join(__dirname, 'client', 'build')));
	app.get('*', (req, res) => {
		res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
	})
}

(
async function () {

	try {
		await mongoose.connect(config.get('ATLAS_URI'), {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useCreateIndex: true
		})
		app.listen(PORT, () => console.log(`Server has been listen port ${PORT}`))
	} catch (e) {
		console.log('Error connection mongoose: ' + e.message)
		process.exit(1);
	}
}
)();

