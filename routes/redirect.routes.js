const {Router} = require('express');
const Link = require('../models/Link.models');
const router = Router();

router.get('/:code', async (req, res) => {
	console.log(req.params)
	try {
		const link = await Link.findOne({code: req.params.code});
		if (link) {
			link.clicks++;
			await link.save();
			return res.redirect(link.from);
		}

		res.status(404).json('Ссылка не найдена');
	} catch (e) {

	}
})

module.exports = router;