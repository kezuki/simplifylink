const {Router} = require('express');
const router = Router();
const config = require('config');
const shortId = require('shortid');
const Link = require('../models/Link.models');
const authMidd = require('../middleware/auth.middleware');

router.post('/generate', authMidd, async (req, res) => {
	try {
		const baseUrl = config.get('BASE_URL');
		const {from} = req.body;
		const code = shortId.generate();

		const existing = await Link.findOne({from});

		if (existing) {
			return res.json({link: existing});
		}

		const to = baseUrl + '/t/' + code;

		const link = new Link({
			code, to, from, owner: req.user.userId
		});

		await link.save();

		res.status(201).json({link});


	} catch (e) {
		res.status(500).json({message: 'Что-то пошло не так, попробуйте позже'})
	}
});

router.get('/', authMidd, async (req, res) => {
	try {
		const links = await Link.find({owner: req.user.userId})
		res.status(200).json(links)

	} catch (e) {
		res.status(500).json({message: 'Что-то пошло не так, попробуйте позже'})
	}
});

router.get('/:id', authMidd, async (req, res) => {
	try {
		const link = await Link.findById(req.params.id);
		res.status(200).json(link);
	} catch (e) {
		res.status(500).json({message: 'Что-то пошло не так, попробуйте позже'})
	}
});

module.exports = router;